require("appmetrics-dash").monitor();

const express = require("express");
const app = express();
const cpfValidator = require("./models/Cpf");

app.get("/", (req, res) =>
  res.send("API para validação de CPF. Exemplo: /validator/?cpf=00000000000")
);
app.get("/validator", (req, res) => {
  const cpf = req.query.cpf;
  const result =
    cpfValidator.checkCorrectSize(cpf) &&
    !cpfValidator.checkAllSameDigits(cpf) &&
    cpfValidator.validateDigit(cpf, 9) &&
    cpfValidator.validateDigit(cpf, 10);

  res.send({
    cpf: cpf,
    result: result
  });
});

app.listen(3000);
